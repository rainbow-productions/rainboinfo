// Function to fetch user's IP address
function getUserIP() {
    return new Promise((resolve, reject) => {
      fetch('https://ipwho.is')
        .then(response => response.json())
        .then(data => {
          if (data.ip) {
            resolve(data.ip);
            console.log('Fetched user ip address. IP: ' + data.ip)
          } else {
            reject('Unable to fetch user IP address.');
          }
        })
        .catch(error => {
          reject(error);
        });
    });
  }
  
  // Function to fetch IP information and update the page
  function fetchIPInformation() {
    getUserIP()
      .then(ipAddress => {
        fetch(`https://ipwho.is/${ipAddress}`)
          .then(response => response.json())
          .then(data => {
            if (data.success) {
              document.getElementById('postal').innerHTML = data.postal;
              console.log('Fetched postal: ' + data.postal)
            } else {
              // Handle the error, e.g., display an error message
              console.error('Error:', data.message);
            }
          })
          .catch(error => {
            // Handle network or other errors
            console.error('Error:', error);
          });
      })
      .catch(error => {
        // Handle errors from getting user IP
        console.error('Error:', error);
      });
  }
  
  // Call the function to fetch IP information when the page loads
  fetchIPInformation();
  